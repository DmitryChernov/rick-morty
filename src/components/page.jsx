import React from 'react'
import Arrow from '../image/arrow.png'

const Page = ({ info, page, handlerChangePage, onChangeNumberPage, keyPress }) => {
    return (
        <div className='container-page'>
            <button className='container-page__button'
                onClick={() => {
                    handlerChangePage("https://rickandmortyapi.com/api/character/?page=1")
                }}

            >1</button>
            <button className='container-page__button'
                onClick={() => {
                    handlerChangePage(info.prev)
                }}>
                <img src={Arrow} className='container-page__prev-icon' />
            </button>
            <input type='text' className='container-page__selected-page'
                onChange={(event) => {
                    onChangeNumberPage(event.target.value)
                }}
                onKeyUp={(event) => {
                    keyPress(event)
                }}
                value={page}></input>
            <button className='container-page__button'
                onClick={() => {
                    handlerChangePage(info.next)
                }}
            >
                <img src={Arrow} className='container-page__next-icon' />
            </button>
            <button className='container-page__button'
                onClick={() => {
                    handlerChangePage(`https://rickandmortyapi.com/api/character/?page=${info.pages}`)
                }}
            >{info.pages}</button>
        </div>
    )
}

export default Page
