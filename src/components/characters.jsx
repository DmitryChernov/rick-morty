import React from 'react'


const Character = ({ item, id }) => {

    return (
        <div className='characrter' key={id}>
            <img src={item.image} className='character__image' alt={item.name} />
            <div className='character__name'>{item.name}</div>
            <div className="character__status">{item.status}</div>
            <div className='character__gender'>{item.gender}</div>
            <div className='character__species'>{item.species}</div>
        </div>
    )
}

const Characters = ({ items }) => {

    return (
        <div className='container-characters'>
            {items.map(item => {
                return (
                    <Character item={item} key={item.id} />
                )
            })}
        </div>
    )
}

export default Characters