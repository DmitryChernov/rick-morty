import React, { useEffect, useState } from "react";

const Filter = ({ filter, gender, species, status, changeFilter }) => {

    return (
        <div className="container-filter">
            <label>Статус</label>
            <select value={status} onChange={(event) => {
                changeFilter('status', event.target.value.toLowerCase())
            }}>

                <option value='alive'>Alive</option>
                <option value='dead'>Dead</option>
            </select>

            <label>Пол</label>
            <select value={gender} onChange={(event) => {
                changeFilter('gender', event.target.value.toLowerCase())
            }}>
                <option value="null">null</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
            </select>

            <label>Вид</label>
            <select value={species} onChange={(event) => {
                changeFilter('species', event.target.value.toLowerCase())
            }}>
                <option value="null">null</option>
                <option value="human">Human</option>
                <option value="alien">Alien</option>
                <option value="humanoid">Humanoid</option>
                <option value="poopybutthole">Poopybutthole</option>
                <option value="mythological">Mythological Creature</option>
                <option value="animal">Animal</option>
                <option value="robot">Robot</option>
                <option value="cronenberg">Cronenberg</option>
                <option value="disease">Disease</option>
            </select>
        </div>
    )
}

export default Filter