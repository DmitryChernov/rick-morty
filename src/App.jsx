import React, { useEffect, useState } from "react"
import Characters from "./components/characters";
import Page from "./components/page";
import Filter from "./components/Filter";

const App = () => {
    const [error, setError] = useState(null);
    const [items, setItems] = useState([]);
    const [isLoaded, setIsLoaded] = useState(false);
    const [page, setPage] = useState(null);
    const [status, setStatus] = useState(null);
    const [gender, setGender] = useState(null);
    const [species, setSpecies] = useState(null);

    useEffect(() => {
        fetch("https://rickandmortyapi.com/api/character")
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setItems(result);
                    setPage(1);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error)
                }
            )
    }, []);
    useEffect(() => {
        handlerChangePage('https://rickandmortyapi.com/api/character/?page=1')
    }, [status, gender, species])

    const handlerChangeFilter = (type, value) => {
        if (type === "gender") {
            value === 'null' ? setGender(null) : setGender(value)
        } else if (type === "status") {
            value === 'null' ? setStatus(null) : setStatus(value)
        } else {
            value === 'null' ? setSpecies(null) : setSpecies(value)
        }
    }

    const onChangeNumberPage = (numberPage) => {
        if ((Number(numberPage) >= 1 && Number(numberPage) <= items.info.pages) || numberPage === '') {
            setPage(numberPage)
        } else {
            alert("Страницы не существует!!!")
        }
    }

    const filter = (url) => {
        if (gender !== null && !url.includes(gender)) {
            url.length > 43 ? url += `&gender=${gender}` : url += `gender=${gender}`
        }
        if (species !== null && !url.includes(species)) {
            url.length > 43 ? url += `&species=${species}` : url += `species=${species}`
        }
        if (status !== null && !url.includes(status)) {
            url.length > 43 ? url += `&status=${status}` : url += `status=${status}`
        }
        return url
    }

    const handlerChangePage = (url) => {
        console.log(url)
        if (!url) {
            alert("Страницы не существует")
        } else {
            url = filter(url)
            console.log(url)
            setIsLoaded(false)
            fetch(url)
                .then(res => res.json())
                .then(
                    (result) => {
                        setIsLoaded(true);
                        setItems(result);
                        setPage(parseInt(url.substr(48, 49)));
                    },
                    (error) => {
                        setIsLoaded(true);
                        setError(error)
                    }
                )
        }
    }

    const keyPress = (key) => {
        if (key.which === 13) {
            handlerChangePage(`https://rickandmortyapi.com/api/character/?page=${page}`)
        }
    }

    if (error) {
        return (
            <div>Ошибка: {error.message}</div>
        )
    } else if (!isLoaded) {
        return (
            <div>Загрузка ...</div>
        )
    } else {
        return (
            <div className="container">
                <div>
                    <Characters items={items.results} />
                    <Page info={items.info} page={page}
                        handlerChangePage={handlerChangePage}
                        onChangeNumberPage={onChangeNumberPage}
                        keyPress={keyPress} />
                </div>
                <Filter filter={filter} gender={gender} species={species} status={status} changeFilter={handlerChangeFilter} />
            </div>
        )
    }
}

export default App
